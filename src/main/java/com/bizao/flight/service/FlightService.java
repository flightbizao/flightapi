package com.bizao.flight.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bizao.flight.model.BrowseRouteModel;
import com.bizao.flight.model.Carriers;
import com.bizao.flight.model.FlightList;
import com.bizao.flight.model.FlightResult;
import com.bizao.flight.model.OutboundLegModel;
import com.bizao.flight.model.Places;
import com.bizao.flight.model.Quotes;
import com.google.gson.Gson;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

@Service
public class FlightService {
	
    private final Logger log = LoggerFactory.getLogger(FlightService.class);

	/**
	 * 
	 * @param origine
	 * @param destination
	 * @param inbound
	 * @param outbound
	 * @param country
	 * @param currency
	 * @param local
	 * @return 
	 */
	public FlightResult getFlightList(String origine,String destination,String inbound,String outbound,String country,String currency,String local) {
		
		List<FlightList> flights=new ArrayList<FlightList>();
		FlightResult flightresult=new FlightResult();
		
		BrowseRouteModel browseRoute=new BrowseRouteModel();
        final Gson gson = new Gson();

		OkHttpClient client = new OkHttpClient();
		String url="https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/";
		HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
		urlBuilder.addPathSegment(country);
		urlBuilder.addPathSegment(currency);
		urlBuilder.addPathSegment(local);
		urlBuilder.addPathSegment(origine);
		urlBuilder.addPathSegment(destination);
		urlBuilder.addPathSegment(outbound);
		urlBuilder.addQueryParameter("inboundpartialdate", inbound);
		
		String urle = urlBuilder.build().toString();

		Request request = new Request.Builder()
				
				.url(urle)
				.get()
				.addHeader("x-rapidapi-key", "b8da5600famshd0bc3ab50581b5fp197887jsn7fe67b20e4c8")
				.addHeader("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com")
				.build();

			try {
				Response response = client.newCall(request).execute();
				if(response.isSuccessful()) {
		            String repbody = response.body().string();
		            browseRoute=gson.fromJson( repbody , BrowseRouteModel.class);
		            if(browseRoute!=null) {
		            	List<Quotes> quotes=new ArrayList<Quotes>();
		            	quotes=browseRoute.getQuotes();
		            	for(Quotes q:quotes) {
		            		FlightList flight=new FlightList();
		            		flight.setPrice(q.getMinPrice());
		            		flight.setDirect(q.getDirect());
		            		OutboundLegModel outboundleg=q.getOutbounled();
		            		String carriersList="";
		            		List<Integer> carriers=outboundleg.getCarrierIds();
		            		for(Integer c:carriers) {
		            			Optional<Carriers> matchingObject = browseRoute.getCarriers().stream().
		            				    filter(cr -> cr.getCarriereId().equals(c.toString())).
		            				    findFirst();
		            			if(matchingObject.isPresent()) {
		            				Carriers carrier=matchingObject.get();
		            				carriersList+=carrier.getName().concat(" ");
		            			}
		            		}
		            		Optional<Places> origin = browseRoute.getPlaces().stream().
	            				    filter(p -> p.getPlaceId().equals(outboundleg.getOriginId())).
	            				    findFirst();
	            		
		            		if(origin.isPresent()){
		            			flight.setOrigin(origin.get().getCityName());
		            		}
		            		
		            		Optional<Places> dest = browseRoute.getPlaces().stream().
	            				    filter(p -> p.getPlaceId().equals(outboundleg.getDestinationId())).
	            				    findFirst();
	            		
		            		if(dest.isPresent()){
		            			flight.setDestination(dest.get().getCityName());
		            		}
		            		
		            		flight.setCarrier(carriersList);
		            		flight.setDepartureDate(outboundleg.getDepartureDate());
		            		flights.add(flight);
		            	}
		            }


				}

				flightresult.setFlights(flights);
			} catch (IOException e) {
				log.error("", e);
			}


		return flightresult;
	}

	
}
