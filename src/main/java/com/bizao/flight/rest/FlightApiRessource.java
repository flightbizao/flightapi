package com.bizao.flight.rest;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bizao.flight.model.FlightResult;
import com.bizao.flight.service.FlightService;
import com.bizao.flight.utils.Constante;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;



@RestController
@RequestMapping("/api")
public class FlightApiRessource {
	
    private final Logger log = LoggerFactory.getLogger(FlightApiRessource.class);
    private final FlightService flightservie;

	 public FlightApiRessource(FlightService flightservie) {
		super();
		this.flightservie = flightservie;
	}
	
	@ApiOperation(value = "Get list of flights between two cities with details about price and departure date ", response = Iterable.class, tags = "getFlightListBetweenTwoCountry")
	@ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Suceess|OK"),
            @ApiResponse(code = 401, message = "not authorized!"), 
            @ApiResponse(code = 403, message = "forbidden!!!"),
            @ApiResponse(code = 404, message = "not found!!!"),
            @ApiResponse(code = 500, message = "Internal server error!!!")})

 
	@GetMapping("/flights")
    public ResponseEntity<FlightResult> getFlightListBetweenTwoCountry(
    		
    		@RequestParam(defaultValue = "anytime") Optional<String> inbound,
    		@RequestParam(defaultValue = "anytime") Optional<String> outbound,
    		@RequestParam(defaultValue = "SFO-sky") String origine,
    		@RequestParam(defaultValue = "LAX-sky") String destination,
    		@RequestParam(defaultValue = "US")String country,
    		@RequestParam(defaultValue = "USD")String currency,
    		@RequestParam(defaultValue = "en-US") String local) {
			
		     FlightResult flightlist=new FlightResult();
			 String inboundValue=Constante.DEFAULT_DATE;
			 String outboundValue=Constante.DEFAULT_DATE;
		
			
			 if(!inbound.isPresent()) {
				 inboundValue=Constante.DEFAULT_DATE;
			 }
			 if(!outbound.isPresent()) {
				 outboundValue=Constante.DEFAULT_DATE;
			 }
			 
			 flightlist=flightservie.getFlightList(origine,destination, outboundValue, inboundValue,country,currency,local);
	         return ResponseEntity.ok().body(flightlist);

	 
    }

}
