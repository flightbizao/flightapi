package com.bizao.flight.model;

import java.util.List;

public class FlightResult {
	
	private List<FlightList> flights;

	public List<FlightList> getFlights() {
		return flights;
	}

	public void setFlights(List<FlightList> flights) {
		this.flights = flights;
	}
	

}
