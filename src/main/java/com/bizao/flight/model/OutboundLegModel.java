package com.bizao.flight.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class OutboundLegModel  {
	
	@SerializedName("CarrierIds")
	private List<Integer> carrierIds;
	
	@SerializedName("OriginId")
	private Long originId;
	
	@SerializedName("DestinationId")
	private Long destinationId;
	
	@SerializedName("DepartureDate")
	private String departureDate;

	public List<Integer> getCarrierIds() {
		return carrierIds;
	}

	public void setCarrierIds(List<Integer> carrierIds) {
		this.carrierIds = carrierIds;
	}

	

	public Long getOriginId() {
		return originId;
	}

	public void setOriginId(Long originId) {
		this.originId = originId;
	}

	public Long getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(Long destinationId) {
		this.destinationId = destinationId;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	@Override
	public String toString() {
		return "OutboundLegModel [carrierIds=" + carrierIds + ", originId=" + originId + ", destinationId="
				+ destinationId + ", departureDate=" + departureDate + "]";
	}

	
	

}
