package com.bizao.flight.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Routes {
	
	
	@SerializedName("Price")
	private Double price;
	
	
	@SerializedName("QuoteDateTime")
	private String quoteDateTime;
	
	@SerializedName("OriginId")
	private Integer originId;
	
	@SerializedName("DestinationId")
	private Integer DestinationId;
	
	@SerializedName("QuoteIds")
	private List<String> quoteIds;
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getQuoteDateTime() {
		return quoteDateTime;
	}
	public void setQuoteDateTime(String quoteDateTime) {
		this.quoteDateTime = quoteDateTime;
	}
	public Integer getOriginId() {
		return originId;
	}
	public void setOriginId(Integer originId) {
		this.originId = originId;
	}
	public Integer getDestinationId() {
		return DestinationId;
	}
	public void setDestinationId(Integer destinationId) {
		DestinationId = destinationId;
	}
	public List<String> getQuoteIds() {
		return quoteIds;
	}
	public void setQuoteIds(List<String> quoteIds) {
		this.quoteIds = quoteIds;
	}
	@Override
	public String toString() {
		return "Routes [price=" + price + ", quoteDateTime=" + quoteDateTime + ", originId=" + originId
				+ ", DestinationId=" + DestinationId + ", quoteIds=" + quoteIds + "]";
	}
	
	

}
