package com.bizao.flight.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Quotes  {
	
	@SerializedName("QuoteId")
	private String quoteId;
	
	@SerializedName("MinPrice")
	private  Long minPrice;
	
	@SerializedName("Direct")
	private Boolean direct;
	
	@SerializedName("QuoteDateTime")
	private String quoteDateTime;
	
	@SerializedName("OutboundLeg")
	private OutboundLegModel outbounled;

	public String getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}

	public Long getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Long minPrice) {
		this.minPrice = minPrice;
	}

	public Boolean getDirect() {
		return direct;
	}

	public void setDirect(Boolean direct) {
		this.direct = direct;
	}

	public String getQuoteDateTime() {
		return quoteDateTime;
	}

	public void setQuoteDateTime(String quoteDateTime) {
		this.quoteDateTime = quoteDateTime;
	}

	public OutboundLegModel getOutbounled() {
		return outbounled;
	}

	public void setOutbounled(OutboundLegModel outbounled) {
		this.outbounled = outbounled;
	}

	@Override
	public String toString() {
		return "Quotes [quoteId=" + quoteId + ", minPrice=" + minPrice + ", direct=" + direct + ", quoteDateTime="
				+ quoteDateTime + ", outbounled=" + outbounled + "]";
	}

	
	
}
