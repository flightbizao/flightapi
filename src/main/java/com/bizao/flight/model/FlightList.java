package com.bizao.flight.model;

public class FlightList {
	
	private Long price;
	private String carrier;
	private String departureDate;
	private Boolean direct;
	private String origin;
	private String destination;
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public Boolean getDirect() {
		return direct;
	}
	public void setDirect(Boolean direct) {
		this.direct = direct;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	

}
