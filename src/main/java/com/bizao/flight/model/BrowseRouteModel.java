package com.bizao.flight.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.google.gson.annotations.SerializedName;


public class BrowseRouteModel {
	 
	
	
    @SerializedName("Quotes")
	private List<Quotes> quotes;
	
    @SerializedName("Carriers")
	private List<Carriers> carriers;
	
    @SerializedName("Places")
	private  List<Places> places;
	
    @SerializedName("Currencies")
	private List<Currencies> currencies;
	
    @SerializedName("Routes")
	private List<Routes> routes;
	
	public List<Quotes> getQuotes() {
		return quotes;
	}
	
	@JsonSetter("Quotes")
	public void setQuotes(List<Quotes> quotes) {
		this.quotes = quotes;
	}
	
	
	public List<Carriers> getCarriers() {
		return carriers;
	}
	public void setCarriers(List<Carriers> carriers) {
		this.carriers = carriers;
	}
	
	
	public List<Places> getPlaces() {
		return places;
	}
	public void setPlaces(List<Places> places) {
		this.places = places;
	}
	
	public List<Currencies> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(List<Currencies> currencies) {
		this.currencies = currencies;
	}
	

	public List<Routes> getRoutes() {
		return routes;
	}
	public void setRoutes(List<Routes> routes) {
		this.routes = routes;
	}
	@Override
	public String toString() {
		return "BrowseRouteModel [quotes=" + quotes + ", carriers=" + carriers + ", places=" + places + ", currencies="
				+ currencies + ", routes=" + routes + "]";
	}
	
	
	

}
