package com.bizao.flight.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Carriers  {
	
	@SerializedName("CarrierId")
	private String carriereId;
	
	@SerializedName("Name")
	private String name;

	public String getCarriereId() {
		return carriereId;
	}

	public void setCarriereId(String carriereId) {
		this.carriereId = carriereId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Carriers [carriereId=" + carriereId + ", name=" + name + "]";
	}
	
	

}
