package com.bizao.flight.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class Places  {
	
	@SerializedName("Name")
	private String name;
	
	@SerializedName("Type")
	private String type;
	
	@SerializedName("PlaceId")
	private Long placeId;
	
	@SerializedName("IataCode")
	private String iataCode;
	
	@SerializedName("SkyscannerCode")
	private String skyscannerCode;
	
	@SerializedName("CityName")
	private String cityName;
	
	@SerializedName("CityId")
	private String cityId;
	
	@SerializedName("CountryName")
	private String countryName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Long placeId) {
		this.placeId = placeId;
	}

	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getSkyscannerCode() {
		return skyscannerCode;
	}

	public void setSkyscannerCode(String skyscannerCode) {
		this.skyscannerCode = skyscannerCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public String toString() {
		return "Places [name=" + name + ", type=" + type + ", placeId=" + placeId + ", iataCode=" + iataCode
				+ ", skyscannerCode=" + skyscannerCode + ", cityName=" + cityName + ", cityId=" + cityId
				+ ", countryName=" + countryName + "]";
	}
	
	
	

}
